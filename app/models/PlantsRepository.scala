package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext

@Singleton
class PlantsRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
	private val dbConfig = dbConfigProvider.get[JdbcProfile]

	// These imports are important, the first one brings db into scope, which will let you do the actual db operations.
	// The second one brings the Slick DSL into scope, which lets you define the table and other queries.
	import dbConfig._
	import profile.api._

	private class PlantTable(tag: Tag) extends Table[Plant](tag, "plants") {
		def acceptedSymbol = column[String]("Accepted Symbol")
		def synonymSymbol = column[String]("Synonym Symbol")
		def symbol = column[String]("SYMBOL")
		def scientificName = column[String]("Scientific Name")
		def hybridGenusIndicator = column[String]("Hybrid Genus Indicator")
		def genus = column[String]("GENUS")
		def hybridSpeciesIndicator = column[String]("Hybrid Species Indicator")
		def species = column[String]("SPECIES")
		def subspeciesPrefix = column[String]("Subspecies Prefix")
		def hybridSubspeciesIndicator = column[String]("Hybrid Subspecies Indicator")
		def subspecies = column[String]("SUBSPECIES")
		def varietyPrefix = column[String]("Variety Prefix")
		def hybridVarietyIndicator = column[String]("Hybrid Variety Indicator")
		def variety = column[String]("VARIETY")
		def subvarietyPrefix = column[String]("Subvariety Prefix")
		def subvariety = column[String]("SUBVARIETY")
		def formaPrefix = column[String]("Forma Prefix")
		def forma = column[String]("FORMA")
		def generaBinomialAuthor = column[String]("Genera/Binomial Author")
		def trinomialAuthor = column[String]("Trinomial Author")
		def quadranomialAuthor = column[String]("Quadranomial Author")
		def questionableTaxonIndicator = column[String]("Questionable Taxon Indicator")
		def parents = column[String]("PARENTS")
		def commonName = column[String]("Common Name")
		def plantsFloristicArea = column[String]("PLANTS Floristic Area")
		def category = column[String]("CATEGORY")
		def genus1 = column[String]("GENUS1")
		def family = column[String]("FAMILY")
		def familySymbol = column[String]("Family Symbol")
		def familyCommonName = column[String]("Family Common Name")
		def order = column[String]("ORDER")
		def subclass = column[String]("SUBCLASS")
		def klass = column[String]("CLASS")
		def subdivision = column[String]("SUBDIVISION")
		def division = column[String]("DIVISION")
		def superdivision = column[String]("SUPERDIVISION")
		def subkingdom = column[String]("SUBKINGDOM")
		def kingdom = column[String]("KINGDOM")
		def itisTsn = column[String]("ITIS TSN")
		def duration = column[String]("DURATION")
		def growthHabit = column[String]("Growth Habit")
		def nativeStatus = column[String]("Native Status")
		def federalNoxiousStatus = column[String]("Federal Noxious Status")
		def federalNoxiousCommonName = column[String]("Federal Noxious Common Name")
		def stateNoxiousStatus = column[String]("State Noxious Status")
		def stateNoxiousCommonName = column[String]("State Noxious Common Name")
		def invasive = column[String]("INVASIVE")
		def federalTEStatus = column[String]("Federal T/E Status")
		def stateTEStatus = column[String]("State T/E Status")
		def stateTECommonName = column[String]("State T/E Common Name")
		def nationalWetlandIndicatorStatus = column[String]("National Wetland Indicator Status")
		def regionalWetlandIndicatorStatus = column[String]("Regional Wetland Indicator Status")

		/**
			* This is the tables default "projection".
			*
			* It defines how the columns are converted to and from the Person object.
			*
			* In this case, we are simply passing the id, name and page parameters to the Person case classes
			* apply and unapply methods.
			*/
		def * = (acceptedSymbol,synonymSymbol,symbol,scientificName,hybridGenusIndicator,genus,
			hybridSpeciesIndicator,species,subspeciesPrefix,hybridSubspeciesIndicator,subspecies,
			varietyPrefix,hybridVarietyIndicator,variety,subvarietyPrefix,subvariety,formaPrefix,
			forma,generaBinomialAuthor,trinomialAuthor,quadranomialAuthor,questionableTaxonIndicator,
			parents,commonName,plantsFloristicArea,category,genus1,family,familySymbol,familyCommonName,
			order,subclass,klass,subdivision,division,superdivision,subkingdom,kingdom,itisTsn,
			duration,growthHabit,nativeStatus,federalNoxiousStatus,federalNoxiousCommonName,stateNoxiousStatus,
			stateNoxiousCommonName,invasive,federalTEStatus,stateTEStatus,stateTECommonName,
			nationalWetlandIndicatorStatus,regionalWetlandIndicatorStatus
		) <> ((Plant.apply _).tupled, Plant.unapply)
	}
}
